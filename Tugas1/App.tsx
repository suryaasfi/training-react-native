import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react'
import Home from './src/Home'
import ClassC from './src/classC';
import Portal from './src/Portal';
import Maps from './src/Maps';
import search from './src/search';
import ToDoList from './src/Component/ToDoList';

const App = () => {
  const Stack = createNativeStackNavigator();
  return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Details" component={ClassC} />
          <Stack.Screen name="Portal" component={Portal} />
          <Stack.Screen name="Maps" component={Maps} />
          <Stack.Screen name="search" component={search} />
          <Stack.Screen name="ToDoList" component={ToDoList} />
        </Stack.Navigator>
      </NavigationContainer>
    );

}


export default App