import {View, Text, ScrollView} from 'react-native';
import React, {useEffect, useState} from 'react';
import axios from 'axios';

const Mps = () => {
  const [databaru, setDatabaru] = useState();
  useEffect(() => {
    getTrx();
  }, []);

  const getTrx = () => {
    var data = '';

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {},
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        const data = Object.entries(response.data);
        setDatabaru(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <View style={{flex: 1}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{alignItems: 'center'}}>
          {databaru?.map((e, index) => {
            return (
              <View
                style={{
                  backgroundColor: 'white',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                  marginTop: 10,
                  border: 10,
                  elevation: 10,
                  borderWidth: 0.5,
                  borderRadius: 50,
                  borderColor: 'aquamarine',
                }}
                key={index}>
                <Text style={{fontWeight: 'bold', color: 'black'}}>
                  NO : {index}
                </Text>
                <Text style={{fontWeight: '600', color: 'pink'}}>
                  ID : {e[0]}
                </Text>
                <Text style={{fontWeight: '600', color: 'red'}}>
                  Tujuan : {e[1].target}
                </Text>
                <Text style={{fontWeight: '600', color: 'gray'}}>
                  Type : {e[1].type}
                </Text>
                <Text style={{fontWeight: '600', color: 'black'}}>
                  Biaya : {e[1].amount}
                </Text>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default Mps;
