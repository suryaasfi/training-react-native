import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';

const ViewStyleProps = ({navigation}) => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      style={{flex: 1}}>
      <View style={styles.container}>
        <View style={styles.top}>
          <View
            onTouchEnd={() => {
              navigation.navigate('search');
            }}
            style={styles.search}>
            <Image
              source={{
                uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Search_font_awesome.svg/1024px-Search_font_awesome.svg.png',
              }}
              style={{
                width: 15,
                height: 20,
                borderLeft: 10,
                resizeMode: 'contain',
              }}
            />
            <TextInput
              placeholder="Mau cari apa di Gojek?"
              style={styles.input}
            />
            <View
              style={{
                width: 130,
                height: 30,
                borderRadius: 50,
                backgroundColor: '#f0f8ff',
              }}>
              <Image
                source={{
                  uri: 'https://icons.veryicon.com/png/o/commerce-shopping/shopping-linear-icon-library/promotion-9.png',
                }}
                style={{width: 25, height: 28, resizeMode: 'contain'}}
              />
            </View>
          </View>
        </View>
        <View style={styles.middle}>
          <Image
            source={{
              uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT35nswUzrMgOKwT_LhHVpyAfBrH97UJLheHg&usqp=CAU.jpg',
            }}
            style={{
              height: 80,
              width: 200,
              resizeMode: 'stretch',
              backgroundColor: '#4169e1',
            }}
          />
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingVertical: 20,
            marginRight: 5,
          }}>
          <View>
            <View
              onTouchEnd={() => {
                navigation.navigate('Details');
              }}
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'blue',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlEur0HRU7U1ESeuy-Zg-lrpm_qIybWMGFonuFyoL1YenDy5ThZs5cCKz8GhVq3K_BWxk&usqp=CAU.jpg',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>GoRide</Text>
            </View>
          </View>

          <View>
            <View
              onTouchEnd={() => {
                navigation.navigate('Portal');
              }}
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'white',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://s.kaskus.id/r480x480/images/fjb/2019/12/20/tmp_phpknari3_1743120_1576787796.jpg',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>GoCar</Text>
            </View>
          </View>

          <View>
            <View
              onTouchEnd={() => {
                navigation.navigate('ToDoList');
              }}
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'white',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://miro.medium.com/max/2400/1*GyGmdWe96bvsG4edAttUPg.png',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>GoFood</Text>
            </View>
          </View>

          <View>
            <View
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'white',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://lelogama.go-jek.com/prime/upload/help/icon/Size40_Kindtile_Colorcolor_Servicegobluebird.png',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>GoBlue</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', alignContent: 'center'}}>
          <View style={styles.bottom} />
          <View style={styles.center} />
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingVertical: 20,
            marginRight: 5,
          }}>
          <View>
            <View
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'white',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjbVn4FQ2JGi9giEXvNkhQnkqnsKVwiGYQTcwl8ZPsGr_ZecnDGfzYQ-AErZyjEvAo07s&usqp=CAU.jpg',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>GoSend</Text>
            </View>
          </View>

          <View>
            <View
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'white',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Mobile_phone_font_awesome.svg/1200px-Mobile_phone_font_awesome.svg.png',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>GoPulsa</Text>
            </View>
          </View>

          <View>
            <View
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'white',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://cdn5.vectorstock.com/i/1000x1000/00/34/trophy-icon-images-vector-34190034.jpg',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>GoPoints</Text>
            </View>
          </View>

          <View>
            <View
              onTouchEnd={() => {
                navigation.navigate('Maps');
              }}
              style={{
                height: 50,
                width: 50,
                backgroundColor: 'white',
                borderWidth: 1,
                overflow: 'hidden',
                borderRadius: 100,
              }}>
              <Image
                source={{
                  uri: 'https://gopay.co.id/icon.png',
                }}
                style={{
                  height: 50,
                  width: 50,
                  resizeMode: 'stretch',
                }}
              />
            </View>
            <View>
              <Text style={{textAlign: 'center'}}>Lainnya</Text>
            </View>
          </View>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    // Background
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: 'white',
    padding: 20,
    margin: 5,
  },
  search: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    paddingLeft: 15,
    alignItems: 'center',
  },
  input: {
    height: 350,
    width: 200,
    color: '#000000',
  },
  top: {
    //Garis Line Atas
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
    height: 30,
    width: 220,
    marginBottom: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderBottomEndRadius: 10,
  },
  middle: {
    // flex: 0.3,
    backgroundColor: 'white',
    borderWidth: 1,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 5,
  },
});

export default ViewStyleProps;
