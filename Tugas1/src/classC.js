import {Text, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';

export default class classC extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
    };
  }
  decrement() {
    this.setState({
      count: this.state.count - 1,
    });
  }
  render() {
    return (
      <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}}>
        <Text>{this.state.count}</Text>
        <View
          style={{justifyContent: 'space-evenly', alignItems: 'center'}}></View>
        <TouchableOpacity
          onPress={() => {
            this.setState({count: this.state.count + 1});
          }}
          style={{
            backgroundColor: 'white',
            paddingHorizontal: 10,
            paddingVertical: 10,
            elevation: 5,
            borderRadius: 10,
            marginTop: 10,
          }}>
          <Text style={{color: 'black', fontWeight: 'bold'}}>INI COBA</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
