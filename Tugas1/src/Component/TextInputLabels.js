import {View, Text, TextInput, StyleSheet} from 'react-native';
import React, {useState} from 'react';

const TextInputLabels = ({
  placeholders,
  label,
  values,
  onChangeTexts,
  TypeKey,
}) => {
  const [localValue, setlocalValue] = useState('');
  return (
    <View>
      <View>
        <Text style={{color: 'black'}}>{label}</Text>
      </View>
      <View style={style.TextInputCon}>
        <TextInput
          placeholder={placeholders}
          value={values}
          onChangeText={text => {
            setlocalValue(text); //local
            onChangeTexts(text); //kirim
          }}
          keyboardType={TypeKey}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  TextInputCon: {
    marginTop: 5,
    borderWidth: 10,
    borderRadius: 20,
    borderColor: 'black',
    width: 415,
  },
});

export default TextInputLabels;
