import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Button,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
} from 'react-native';

const image = {
  uri: 'https://p4.wallpaperbetter.com/wallpaper/367/257/149/anime-anime-girls-digital-art-artwork-2d-hd-wallpaper-preview.jpg',
};
const TodoList = () => {
  const [todo, setTodo] = useState('');
  const [dataArray, setDataArray] = useState([]);
  const Delete = gagal => {
    setDataArray(dataArray.filter((data, index) => index !== gagal));
  };

  const addTodo = () => {
    setDataArray([...dataArray, todo]);
    setTodo('');
  };
  console.log(dataArray);
  return (
    <SafeAreaView style={{flex: 1}}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'height' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 0}
        style={{flex: 1}}>
        <View style={styles.container}>
          <ImageBackground
            source={image}
            resizeMode="stretch"
            style={styles.image}>
            <ScrollView>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginLeft: 10,
                  marginRight: 10,
                  // textDecorationColor: 'white',
                }}>
                <TextInput
                  style={styles.input}
                  value={todo}
                  onChangeText={setTodo}
                  placeholder="                 Masukan Nama"
                  placeholderTextColor={'white'}
                />
                <TouchableOpacity style={styles.add} onPress={addTodo}>
                  <Text
                    style={{
                      alignItems: 'center',
                      paddingLeft: 8,
                      paddingTop: 3,
                      fontSize: 10,
                    }}>
                    ADD
                  </Text>
                </TouchableOpacity>
              </View>
              {dataArray.map((t, i) => (
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    justifyContent: 'space-between',
                    width: '80%',
                    marginTop: 10,
                    borderRadius: 20,
                    borderWidth: 5,
                    borderColor: 'white',
                  }}>
                  <Text key={i} style={styles.todo}>
                    {t}
                  </Text>
                  <TouchableOpacity
                    style={styles.delete}
                    onPress={() => {
                      Delete(i);
                    }}>
                    <Text style={{color: 'white'}}>Delete</Text>
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
          </ImageBackground>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  image: {
    flex: 1,
    width: 412,
    resizeMode: 'stretch',
    left: 0,
    top: 0,
    // width: Dimensions.get('window').width,
    // height: Dimensions.get('window').height,
    // justifyContent: 'center',
  },
  input: {
    width: 250,
    height: 40,
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 100,
    marginVertical: 20,
    paddingLeft: 15,

    // backgroundColor: 'white',
  },
  add: {
    width: 60,
    height: 30,
    borderRadius: 10,
    backgroundColor: 'white',
    padding: 11,
    paddingTop: 5,
    paddingBottom: 5,
    fontStyle: 50,
    marginLeft: 10,
  },
  todo: {
    fontSize: 18,
    marginVertical: 10,
    textDecorationColor: 'white',
    justifyContent: 'center',
    alignSelf: 'stretch',
    paddingHorizontal: 10,
    paddingBottom: 5,
    color: 'black',
  },
  delete: {
    alignSelf: 'center',
    padding: 3,
    borderWidth: 4,
    borderRadius: 10,
    marginRight: 10,
    paddingBottom: 5,
    borderColor: 'red',
  },
});

export default TodoList;
