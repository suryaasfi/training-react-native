import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import React, {useState} from 'react';
import TextInputLabel from './Component/TextInputLabels';
import axios from 'axios';
import SelectDropdown from 'react-native-select-dropdown';

const image = {uri: 'https://i.imgur.com/8POC9j9.jpg'};

const Portal = () => {
  const [massage, setMassage] = useState('');
  const [nominal, setNominal] = useState(0);
  const [tujuan, setTujuan] = useState('');
  const [pilihan, setPilihan] = useState('');

  const type = ['Isi Pulsa', 'Transfer'];

  const Rupiah = x => {
    if (x) {
      return x
        .toString()
        .replace(/\./g, '')
        .replace(/(\d)(?=(\d{3})+$)/g, '$1.');
    }
  };
  const Bongkar = async () => {
    let hapusTitik = nominal.toString().split('.').join('');
    let finish = Number(hapusTitik);
    await HitApi(finish);
  };

  const HitApi = async price => {
    let data = JSON.stringify({
      amount: price,
      sender: 'NMalhNh5LPYMCq7uuUh',
      target: tujuan,
      type: pilihan,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log('success', JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log('error', error);
      });
  };

  return (
    <View style={styles.container}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <Text style={styles.text}>Inside</Text>
        <View style={styles.nominaL}>
          <TextInputLabel
            label={'Nominal'}
            placeholders={'Masukan Nilai'}
            values={nominal}
            onChangeTexts={text => {
              setNominal(Rupiah(text || 0));
            }}
            //   TypeKey={'number-pad'}
          />
        </View>
        {/* <View style={styles.tujuaN}> */}
        <TextInputLabel
          label={'Tujuan'}
          placeholders={'Masukkan Nama'}
          values={tujuan}
          onChangeTexts={text => {
            setTujuan(text);
          }}
          //   TypeKey={'number-pad'}
        />
        <View style={styles.dropdown}>
          <SelectDropdown
            data={type}
            onSelect={selectedItem => {
              setPilihan(selectedItem);
              console.log(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              // text represented after item is selected
              // if data array is an array of objects then return selectedItem.property to render after item is selected
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              // text represented for each item in dropdown
              // if data array is an array of objects then return item.property to represent item in dropdown
              return item;
            }}
            dropdownOverlayColor={(selectedItem, index) => {
              return selectedItem;
            }}
          />
          {/* </View> */}
        </View>
        <View style={styles.button}>
          <TouchableOpacity
            onPress={() => {
              Bongkar();
            }}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>Enter</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },
  nominaL: {
    // marginTop: 5,
    marginBottom: 20,
    backgroundColor: 'aqua',
    borderColor: 'white',
  },
  tujuaN: {
    marginBottom: 20,
    backgroundColor: 'aqua',
    borderColor: 'white',
  },
  text: {
    color: 'white',
    fontSize: 10,
    lineHeight: 10,
    fontWeight: 'bold',
    textAlign: 'center',
    // backgroundColor: '#000000c0',
  },
  dropdown: {
    alignItems: 'center',
    borderRadius: 50,
    color: 'green',
    fontFamily: '',
  },
  button: {
    backgroundColor: 'aqua',
    alignItems: 'center',
    marginHorizontal: 15,
    paddingVertical: 10,
    marginTop: 20,
    borderRadius: 100,
    elevation: 5,
  },
});

export default Portal;
